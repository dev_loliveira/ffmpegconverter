
from unittest import TestCase
from converter import Converter


class ConverterTest(TestCase):

    def setUp(self):
        self.converter = Converter('')

    def test_correct_assoc_media_index_with_episode_number(self):
        input01, expected01 = ['file01', 'file02'], ['01.mkv', '02.mkv']
        input02, expected02 = ['some02randomfile'], ['02.mkv']
        input03, expected03 = ['file with space 09'], ['09.mkv']

        self.assertEqual(expected01, self.converter.extract_output_names(input01))
        self.assertEqual(expected02, self.converter.extract_output_names(input02))
        self.assertEqual(expected03, self.converter.extract_output_names(input03))
