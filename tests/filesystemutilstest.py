
from unittest import TestCase
from fileutils import FileSystemUtils


class FileSystemUtilsTest(TestCase):

    def setUp(self):
        self.klass = FileSystemUtils()

    def test_media_number_should_return_the_correct_episode_number(self):
        input01, expected_output01 = 'dummyfile01', '01'
        input02, expected_output02 = 'number01file', '01'
        input03, expected_output03 = '05inthebeginning', '05'
        input04, expected_output04 = 'episode number 9.avi', '09'
        input05, expected_output05 = 'episode number 13', '13'

        self.assertEquals(expected_output01, self.klass.media_number(input01))
        self.assertEquals(expected_output02, self.klass.media_number(input02))
        self.assertEquals(expected_output03, self.klass.media_number(input03))
        self.assertEquals(expected_output04, self.klass.media_number(input04))
        self.assertEquals(expected_output05, self.klass.media_number(input05))
