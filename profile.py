import os


class ProfileReader(object):
    FILE = '.profile'

    @staticmethod
    def parse(file_name=None):
        file_name = file_name if file_name is not None else ProfileReader.FILE
        _abs = os.path.join(os.path.dirname(__file__), ProfileReader.FILE)
        with open(_abs) as handle:
            data = handle.read()

        values = []
        for line in data.split('\n'):
            if not line: continue
            key, value = line.split('=')
            values.append( (key, value) )
        return values
