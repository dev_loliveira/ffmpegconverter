
import re
from subprocess import Popen, PIPE


class FFProbeAPI(object):
    CMD = 'ffprobe -i {}'

    def __init__(self, path):
        self.path = path
        self.ffprobe_data = Popen(FFProbeAPI.CMD.format(path).split(' '), stdout=PIPE, stderr=PIPE)

    def getStreams(self):
        data = self.ffprobe_data.stderr.read()
        values_dict = dict()
        for line in data.split('\n'):
            stream_match = re.match(r'Stream #\d:\d', line.strip())
            if stream_match is not None:
                stream_type = ''
                stream_number = ''
                stream_info = ''
                type_match = re.match(r"^.*:(.*):.*$", line)
                stream_type_groups = re.search(r': (\w*):', line).groups()
                stream_number_groups = re.search(r'(#\d:\d).*', line).groups()
                stream_info_groups = re.search('Stream #\d:\d(\(\w*\))', line)

                if len(stream_type_groups) > 0 and len(stream_number_groups) > 0:
                    stream_type = stream_type_groups[0]
                    stream_number = stream_number_groups[0].replace('#', '')

                if stream_info_groups and len(stream_info_groups.groups()) > 0:
                    stream_info = stream_info_groups.groups()[0] \
                                   .replace('(', '') \
                                   .replace(')', '')

                if stream_type not in values_dict:
                    values_dict[stream_type] = list()

                values_dict[stream_type].append(
                    (stream_number, stream_info)
                )
        return values_dict

    def getData(self):
        data = {
            'streams': self.getStreams()
        }

        return data
