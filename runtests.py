
import unittest
from tests.convertertest import ConverterTest
from tests.filesystemutilstest import FileSystemUtilsTest


klass = [
    ConverterTest, FileSystemUtilsTest
]


for _klass in klass:
    tests = unittest.TestLoader().loadTestsFromTestCase(_klass)
    runner = unittest.TextTestRunner(verbosity=2).run(tests)
