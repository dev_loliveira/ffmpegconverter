import sys
import os
from subprocess import Popen, PIPE
from fileutils import FileSystemUtils, NormalizeMedias, MediaStreams
from profile import ProfileReader
from ffprobe.api import FFProbeAPI


class Converter(FileSystemUtils):
    OUTPUT_DIR = 'converted'
    AUDIORATE = 22050
    BITRATE = 2048
    VIDEO_CODEC = '-c:v libx264'
    SUBS_CODEC = '-c:s copy'
    REQUIRED_ARGS = (
        '-ar %d -b %dk' % (AUDIORATE, BITRATE),
    )
    OPTIONAL_ARGS = (
        '2> /dev/null',
    )

    def __init__(self, directory):
        self.directory = directory
        self.rules = ()

    def input_files(self):
        valid_extensions = ['mkv', 'avi' ]
        files = os.listdir(self.directory)
        result = []

        for f in files:
            abs_p = os.path.join(self.directory, f)
            if not os.path.isfile(abs_p):
                continue
            elif f.split('.')[-1] not in valid_extensions:
                continue
            result.append(self.correct_path(f))

        result.sort()
        return result

    def extract_output_names(self, files):
        assoc = []
        for f in files:
            f_number = self.media_number(f)
            f_name = '%s.mkv' % f_number
            assoc.append(f_name)
        return assoc

    def base_command(self, file_name):
        cmd = 'ffmpeg -i %s {v_codec} {s_codec}'.format(**{
            'v_codec': Converter.VIDEO_CODEC,
            's_codec': Converter.SUBS_CODEC
        })
        media_streams = FFProbeAPI(file_name).getStreams()
        streams_map = []
        # 0: Was applied, boolean
        # 1: Value of the filter used, str
        rules_applied = {
            f_type: [False, ''] for f_type, f_value in self.rules
        }
        for stream in media_streams:
            s_type = stream

            for s_number, s_value in media_streams[stream]:
                for f_stream in self.rules:
                    f_type, f_value = f_stream[0], f_stream[1]
                    include = False
                    if f_type == s_type:
                        rules_applied[f_type][1] = f_value
                        if f_value == '*':
                            include = True
                        else:
                            for f_value in f_value.split(','):
                                if s_value == f_value:
                                    include = True
                    if include:
                        rules_applied[f_type][0] = True
                        streams_map.append('-map %s' % s_number)
                    else:
                        rules_applied[f_type][1] = f_value

        # Caso algum stream nao tenha sido incluso apos a aplicacao dos
        # filtros, ele sera copiado
        for stream, data in rules_applied.items():
            was_applied, f_value = data
            if not was_applied:
                msg = '%s "%s" was not found. Copying existing instead.'
                print(msg % (stream, rules_applied[stream][1]))
                s_number = media_streams[stream][0][0]
                streams_map.append('-map %s' % s_number)

        streams_map = self.sorted_array(streams_map)
        # Adiciona os streams mapeados em funcao dos filtros selecionados
        cmd += ' '
        cmd += ' '.join(streams_map)

        cmd = cmd + ' ' + ' '.join(Converter.REQUIRED_ARGS)
        cmd = cmd + ' %s '
        cmd = cmd + ' '.join(Converter.OPTIONAL_ARGS)
        return cmd

    def confirm_order(self, files, output_files):
        for i in xrange(len(output_files)):
            print('%s\t\t-\t\t%s' % (output_files[i], files[i]))
        choice = raw_input('Is this order ok ? (y/n) ')

        return choice.lower() == 'y'

    def start(self):
        base_output_dir = os.path.join(self.directory, Converter.OUTPUT_DIR)
        if not os.path.exists(base_output_dir):
            print('Creating output directory')
            os.mkdir(base_output_dir)

        files = self.input_files()
        files.sort()

        s_i = self.episode_number(files[0])
        l_i = self.episode_number(files[-1]) + 1
        output_names = self.extract_output_names(files)
        if self.confirm_order(files, output_names):
            for f in files:
                output_name = output_names.pop(0)
                output_path = Converter.OUTPUT_DIR + os.sep + output_name
                if os.path.exists(output_path):
                    print('[ %s ]\tAlready exists.' % output_path)
                    continue
                cmd = self.base_command(f) % (f, output_path)
                sys.stdout.write('\rConverting : %s' % output_name)
                sys.stdout.flush()
                os.system(cmd)
        sys.stdout.write('\n')

    def sorted_array(self, array):
        tmp_array = list()
        i = 0
        j = 1
        while j < len(array):
            a, b = array[i], array[j]
            if b < a:
                array[i] = b
                array[j] = a
                i, j = 0, 1
            else:
                i, j = i+1, i+2
        return array


if __name__ == '__main__':
    directory = os.path.abspath('.')
    converter = Converter(directory)
    converter.rules = (ProfileReader.parse())
    converter.start()
