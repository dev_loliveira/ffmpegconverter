import os
from subprocess import Popen, PIPE


class FileSystemUtils(object):

    def correct_path(self, path):
        return path.replace(' ', '\\ ') \
                   .replace('[', '\[')  \
                   .replace(']', '\]')  \
                   .replace(';', '\;')

    def episode_number(self, f_path):
        f_name = f_path.split(os.sep)[-1]
        i = 0
        digits = []
        for c in f_name:
            if c.isdigit():
                for j in xrange(i, len(f_name)):
                    c2 = f_name[j]
                    if c2.isdigit():
                        digits.append(c2)
                    else:
                        break
                break
            i += 1
        return int(''.join(digits))

    def media_number(self, path):
        name = path.split(os.sep)[-1]
        digits = []
        found = False
        for c in name:
            if c.isdigit():
                digits.append(c)
                found = True
            elif found:
                break

        number = ''.join(digits)
        return number if len(number) > 1 else '0' + number


class NormalizeMedias(FileSystemUtils):

    def __init__(self, directory):
        self.directory = directory

    def start(self):
        medias = os.listdir(self.directory)
        for media in medias:
            abs_path = os.path.join(self.directory, self.correct_path(media))
            if os.path.isdir(abs_path):
                continue
            else:
                extension = media.split('.')[-1]
                number = self.media_number(media)
                if number is None:
                    n_output = '%s_converted.%s' % (media, extension)
                else:
                    n_output = '%s.%s' % (number, extension)
                p_output = os.path.join(self.directory, n_output)
                os.system('mv %s %s' % (abs_path, p_output))


class MediaStreams(FileSystemUtils):

    def __init__(self, media_path):
        self.media_path = media_path.replace('\\', '')
        self.grep_stdout = self._grep_stdout()

    def _grep_stdout(self):
        ffprobe = Popen(['ffprobe', self.media_path], stdout=PIPE, stderr=PIPE)
        grep = Popen(['grep', 'Stream #'], stdin=ffprobe.stderr, stdout=PIPE)
        return grep.stdout.read()

    @property
    def streams(self):
        streams = []
        for line in self.grep_stdout.split('\n'):
            if not line:
                continue
            blocks = line.strip().split(' ')
            s_number = ':'.join(blocks[1].replace('#', '').split(':')[0:2])
            s_type = blocks[2].replace(':', '').strip()
            s_language = None

            if s_number.find('(') >= 0:
                s_language = s_number[s_number.find('(') + 1 : s_number.find(')')]
                s_number = s_number[0:3]

            streams.append((s_number, s_type, s_language))
        return streams
